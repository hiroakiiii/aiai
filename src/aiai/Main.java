package aiai;

/** 童謡アイアイを表示するプログラム
 *
 *
 */

public class Main {

	/**アイアイを表示
	 * @return
	 *
	 */

	public static void main(String[] args){
		execute();
	}

	public static void execute(){
		String lyric="";

		//1番
		lyric += repeatMessage(sayAiai(),2);
		lyric += newLine();
		lyric += sayMonkey();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),2);
		lyric += newLine();
		lyric += saySouth();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),4);
		lyric += newLine();
		lyric += sayLongTail();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),2);
		lyric += newLine();
		lyric += sayMonkey();

		//２番
		lyric += newLine();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),2);
		lyric += newLine();
		lyric += secondSayMonkey();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),2);
		lyric += newLine();
		lyric += secondSaySouth();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),4);
		lyric += newLine();
		lyric += secondSayLongTail();
		lyric += newLine();
		lyric += repeatMessage(sayAiai(),2);
		lyric += newLine();
		lyric += secondSayMonkey();



		//以降、自分のメソッドを使ってアイアイを完成させてください。

		System.out.println(lyric);

	}



	public static String sayAiai(){
		return "アイアイ（アイアイ）";
	}

	/**同じメッセージを複数回表示
	 * @param msg
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg,int count){
		String repeatMsg ="";
		for(int i=0; i<count; i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**改行する
	 * @return
	 */

	public static String newLine(){
		return "\n";
	}

	/**おさるさんだよを表示
	 * @return
	 */

	public static String sayMonkey(){
		return "おさるさんだよ";
	}
	/**みなみのしまを表示
	 * @return
	 */
	public static String saySouth(){
		return "みなみのしまの";
	}

	/**しっぽのながいを表示
	 * @return
	 */

	public static String sayLongTail(){
		return "しっぽのながい";

	}

	/**アイアイを歌う処理
	 *
	 */


	/**メインメソッド
	 *@param args
	 */


	public static String secondSayMonkey() {
		// TODO 自動生成されたメソッド・スタブ
		return "おさるさんだね";
	}

	public static String secondSaySouth() {
		// TODO 自動生成されたメソッド・スタブ
		return "きのはのおうち";
	}

	public static String secondSayLongTail() {
		// TODO 自動生成されたメソッド・スタブ
		return "おめめのまるい";
	}

}

